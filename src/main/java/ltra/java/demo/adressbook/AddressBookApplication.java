package ltra.java.demo.adressbook;

import ltra.java.demo.adressbook.core.models.AddressBook;
import ltra.java.demo.adressbook.core.models.Contact;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication
public class AddressBookApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run( AddressBookApplication.class, args );
    }
}
