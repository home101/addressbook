package ltra.java.demo.adressbook.core.models;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class AddressBook
{
    private String                     id;
    private String                     name;
    private HashMap< String, Contact > contacts;

    public AddressBook()
    {
    }

    public AddressBook( String name )
    {
        String uuidRandom = UUID.randomUUID().toString();
        this.id = uuidRandom.split( "-" )[0];
        this.name = name;
        this.contacts = new HashMap<>();
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public HashMap< String, Contact > getContacts()
    {
        return contacts;
    }

    public void setContacts( HashMap< String, Contact > contacts )
    {
        this.contacts = contacts;
    }
}
