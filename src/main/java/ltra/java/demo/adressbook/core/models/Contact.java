package ltra.java.demo.adressbook.core.models;

import java.util.Date;
import java.util.UUID;

public class Contact
{
    private String id;
    private String name;
    private String phone;

    public Contact()
    {
    }

    public Contact( String name, String phone )
    {
        String uuidRandom = UUID.randomUUID().toString();
        this.id = uuidRandom.split( "-" )[0];
        this.name = name;
        this.phone = phone;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }


    public String getPhone()
    {
        return phone;
    }

    public void setPhone( String phone )
    {
        this.phone = phone;
    }

}
