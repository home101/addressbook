package ltra.java.demo.adressbook.core;

import ltra.java.demo.adressbook.core.models.AddressBook;

import java.util.HashMap;
import java.util.List;

public interface IAddressBookRepository
{
    AddressBook getAddressBook( String id );

    HashMap<String, AddressBook> getAllAddressBooks();

    AddressBook addAddressBook( AddressBook addressBook );

    void removeAddressBook( String id );

    AddressBook updateAddressBook( AddressBook addressBook );
}
