package ltra.java.demo.adressbook.controllers;

import ltra.java.demo.adressbook.core.IAddressBookRepository;
import ltra.java.demo.adressbook.core.models.AddressBook;
import ltra.java.demo.adressbook.core.models.Contact;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin( origins = "http://localhost:4200" )
@RestController
@RequestMapping( value = "/api/address-books" )
public class AddressBooksController
{
    private IAddressBookRepository _repository;

    public AddressBooksController( IAddressBookRepository repository )
    {
        this._repository = repository;
        seedData();
    }

    private void seedData()
    {
        AddressBook personal = new AddressBook( "Personal" );
        personal.setContacts( createContacts() );
        _repository.addAddressBook( personal );

        AddressBook social = new AddressBook( "Social" );
        social.setContacts( createContacts() );
        _repository.addAddressBook( social );

        AddressBook forDelete = new AddressBook( "for delete" );
        forDelete.setContacts( createContacts() );
        _repository.addAddressBook( forDelete );
    }

    private HashMap< String, Contact > createContacts()
    {
        HashMap< String, Contact > contacts = new HashMap<>();
        Contact                    one      = new Contact( "Joe", "1111-1111" );
        Contact                    two      = new Contact( "Anna", "2222-2222" );
        Contact                    three    = new Contact( "Jack", "3333-3333" );
        contacts.put( one.getId(), one );
        contacts.put( two.getId(), two );
        contacts.put( three.getId(), three );
        return contacts;
    }

    @RequestMapping( value = "/", method = RequestMethod.GET )
    public HashMap< String, AddressBook > getAllAddressBooks()
    {
        return _repository.getAllAddressBooks();
    }

    @RequestMapping( value = "/add", method = RequestMethod.POST )
    public ResponseEntity< ? > addAddressBook( @RequestParam( value = "name" ) String name )
    {
        try
        {
            AddressBook addressBook = new AddressBook( name );
            AddressBook result      = _repository.addAddressBook( addressBook );
            if ( result == null ) throw new Exception( "Could not create address book for: " + name );
            return new ResponseEntity< Object >( result, HttpStatus.CREATED );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.BAD_REQUEST );
        }
    }

    @RequestMapping( value = "/update", method = RequestMethod.PUT )
    public ResponseEntity< ? > updateAddressBook( @RequestBody AddressBook addressBookResource ) throws Exception
    {
        try
        {
            AddressBook addressBook = _repository.updateAddressBook( addressBookResource );
            if ( addressBook == null )
                throw new Exception( "Address book with id: " + addressBookResource.getId() + "does not exists." );
            return new ResponseEntity< Object >( addressBook, HttpStatus.OK );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @RequestMapping( value = "/delete/{id}", method = RequestMethod.DELETE )
    public ResponseEntity< ? > deleteAddressBook( @PathVariable String id ) throws Exception
    {
        try
        {
            AddressBook addressBook = _repository.getAddressBook( id );
            if ( addressBook == null )
                throw new Exception( "Address book with id: " + id + " does not exists." );

            _repository.removeAddressBook( id );
            return new ResponseEntity< Object >( addressBook, HttpStatus.OK );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public ResponseEntity< ? > getAddressBook( @PathVariable String id ) throws Exception
    {
        try
        {
            AddressBook addressBook = _repository.getAddressBook( id );
            if ( addressBook == null )
                throw new Exception( "Address book with id: " + id + "does not exists." );

            return new ResponseEntity< Object >( addressBook, HttpStatus.OK );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @RequestMapping( value = "/{addressBookId}/contacts/", method = RequestMethod.GET )
    public HashMap< String, Contact > getAllContacts( @PathVariable( "addressBookId" ) String addressBookId )
    {
        return _repository.getAddressBook( addressBookId ).getContacts();
    }

    @RequestMapping( value = "/{addressBookId}/contacts/{contactId}", method = RequestMethod.GET )
    public ResponseEntity< ? > getContact(
            @PathVariable( "addressBookId" ) String addressBookId,
            @PathVariable( "contactId" ) String contactId
    )
    {
        try
        {
            Contact contact = _repository.getAddressBook( addressBookId )
                                         .getContacts().get( contactId );
            if ( contact == null )
                throw new Exception( "Address book: " + addressBookId + " does not have contact with id: " + contactId );

            return new ResponseEntity< Object >( contact, HttpStatus.OK );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

    @RequestMapping( value = "/{addressBookId}/contacts/add", method = RequestMethod.POST )
    public ResponseEntity< ? > addContact(
            @PathVariable( "addressBookId" ) String addressBookId,
            @RequestParam( value = "name" ) String name,
            @RequestParam( value = "phone" ) String phone
    )
    {
        try
        {
            Contact                    contact  = new Contact( name, phone );
            HashMap< String, Contact > contacts = _repository.getAddressBook( addressBookId ).getContacts();
            contacts.put( contact.getId(), contact );
            _repository.getAddressBook( addressBookId ).setContacts( contacts );

            Contact result = _repository.getAddressBook( addressBookId ).getContacts().get( contact.getId() );

            if ( result == null )
                throw new Exception( "Could not create contact for address book id: " + addressBookId );

            return new ResponseEntity< Object >( result, HttpStatus.CREATED );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.BAD_REQUEST );
        }
    }

    @RequestMapping( value = "/{addressBookId}/contacts/update", method = RequestMethod.PUT )
    public ResponseEntity< ? > updateContact(
            @PathVariable( "addressBookId" ) String addressBookId,
            @RequestBody Contact contactResource
    )
    {
        try
        {
            Contact contact = _repository.getAddressBook( addressBookId )
                                         .getContacts().get( contactResource.getId() );

            if ( contact == null )
                throw new Exception( "Contact: " + contactResource + " does not exists." );


            HashMap< String, Contact > contacts = _repository.getAddressBook( addressBookId ).getContacts();
            contacts.put( contactResource.getId(), contactResource );

            _repository.getAddressBook( addressBookId ).setContacts( contacts );

            Contact result = _repository.getAddressBook( addressBookId )
                                        .getContacts().get( contactResource.getId() );

            return new ResponseEntity< Object >( result, HttpStatus.OK );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.BAD_REQUEST );
        }
    }

    @RequestMapping( value = "/{addressBookId}/contacts/delete/{contactId}", method = RequestMethod.DELETE )
    public ResponseEntity< ? > deleteContact(
            @PathVariable( "addressBookId" ) String addressBookId,
            @PathVariable( "contactId" ) String contactId
    )
    {
        try
        {
            Contact result;
            if ( _repository.getAddressBook( addressBookId ).getContacts().containsKey( contactId ) )
            {
                HashMap< String, Contact > contacts = _repository.getAddressBook( addressBookId ).getContacts();
                result = contacts.get( contactId );
                contacts.remove( contactId );
                _repository.getAddressBook( addressBookId ).setContacts( contacts );
            }
            else
                throw new Exception( "Contact with id: " + contactId + " does not exists." );

            return new ResponseEntity< Object >( result, HttpStatus.OK );
        }
        catch ( Exception ex )
        {
            return new ResponseEntity< Object >( ex.getMessage(), HttpStatus.BAD_REQUEST );
        }
    }
}
