package ltra.java.demo.adressbook.persistence;

import ltra.java.demo.adressbook.core.IAddressBookRepository;
import ltra.java.demo.adressbook.core.models.AddressBook;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service( "addressBookRepository" )
public class AddressBookRepository implements IAddressBookRepository
{
    private HashMap< String, AddressBook > _addressBooks;

    public AddressBookRepository()
    {
        this._addressBooks = new HashMap<>();
    }

    @Override
    public AddressBook getAddressBook( String id )
    {
        return _addressBooks.get( id );
    }

    @Override
    public HashMap< String, AddressBook > getAllAddressBooks()
    {
        return _addressBooks;
    }

    @Override
    public AddressBook addAddressBook( AddressBook addressBook )
    {
        _addressBooks.put( addressBook.getId(), addressBook );
        return getAddressBook( addressBook.getId() );
    }

    @Override
    public void removeAddressBook( String id )
    {
        _addressBooks.remove( id );
    }

    @Override
    public AddressBook updateAddressBook( AddressBook addressBook )
    {
        _addressBooks.put( addressBook.getId(), addressBook );
        return getAddressBook( addressBook.getId() );
    }
}
