# Reece Branch Manager Address Book demo
This program is best to be used with it's front-end Angular application `address-book`.

## IMPORTANT: `address-book/update` require JSON format matching AddressBook model (example below) if not used with `address-book` front-end
## You can also copy the format from doing a GET
Example format: 
{
    "id": "247288ad",
    "name": "address book",
    "contact": {
        "d8d0cdd2": {
            "id": "d8d0cdd2",
            "name": "Joe",
            "phone": "1234"
        }, 
        "a04a9d0d": {
            "id": "a04a9d0d",
            "name": "Hellen",
            "phone": "3214"
        }   
    }
}


## To run
- Run with Java Spring
- local url must be `http://localhost:8080` to be able to use Angular `address-book`
- This project allow cross-origin for `http://localhost:4200`

## API endpoints (try with Chrome extension postman)
- View all address books: `http://localhost:8080/api/address-books/`
- View an address book: `http://localhost:8080/api/address-books/<address-book-id>`
- Delete: `http://localhost:8080/api/address-books/delete<address-book-id>`
- Add: `http://localhost:8080/api/address-books/add` and supply `name` param.
- Update: `http://localhost:8080/api/address-books/update` and supply AddressBook JSON body matching with AddressBook model.


- View all contacts: `http://localhost:8080/api/address-books/<address-book-id>/contacts/`
- View a contact: `http://localhost:8080/api/address-books/<address-book-id>/contacts/<contact-id>/`
- Delete: `http://localhost:8080/api/address-books/<address-book-id>/contacts/<contact-id>delete`
- Update: `http://localhost:8080/api/address-books/<address-book-id>/contacts/update` and supply contact JSON
- Add: `http://localhost:8080/api/address-books/<address-book-id>/contacts/add` and supply `name` and `phone` params